#!/bin/bash
#SBATCH --job-name=nextflow_sarek_sarc_aric_am
#SBATCH --output=slurm-%x-%A_%a.out
#SBATCH --partition=master-worker
#SBATCH --ntasks=1
#SBATCH --time=120:00:00
#SBATCH --mail-user=isaac.wade@icr.ac.uk
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

export SINGULARITY_CACHEDIR="/data/scratch/DGE/DUDGE/PREDIGEN/programs/nfcore_pipelines/singularity_cache"
export NXF_SINGULARITY_CACHEDIR="/data/scratch/DGE/DUDGE/PREDIGEN/programs/nfcore_pipelines/singularity_cache"
export LSB_DEFAULTPROJECT="prepay-pretrgen"
export TOWER_ACCESS_TOKEN="eyJ0aWQiOiAzODE4fS45Y2YxYWI2YjFhYjFiYTliMmU1NWQ4NTBlOTA2MDBjNzYxNDYyMTdm"
export NXF_VER=20.10.0
export NXF_OPTS='-Xms1g -Xmx4g'

module load Nextflow/20.10.0

#Set Variables
rundir="/data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sarek_prep/outputs_IW/aric_batch13"
outdir="/data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarek_results/sarcoma/aric/aric_batch13"
sample_sheet="/data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sarek_prep/aric_am_sample_sheet.tsv"
fastq_backup="am"

#Other Variable[s]
batch=$( echo $outdir | awk '{print $NF}'  FS=/ )

mkdir -p $rundir
cd $rundir
mkdir -p $outdir

nextflow \
run nf-core/sarek -r 2.7 \
--input $sample_sheet \
--outdir  $outdir \
--target_bed /data/scratch/DGE/DUDGE/PREDIGEN/shared/bed/all_coding_exons_20pad_5UTR_sorted_merged.bed \
--genome GRCh38 \
--step mapping \
--intervals /data/scratch/DGE/DUDGE/PREDIGEN/iwade/bedtools-work/sarek_intervals_test3_merged.bed \
--tools HaplotypeCaller \
--trim_fastq \
--generate_gvcf \
--skip_qc BCFtools,samtools,vcftools \
-profile singularity \
-with-tower \
-with-singularity \
--igenomes_base /data/reference-data/iGenomes \
-resume \
-c /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarek-master/conf/icr_alma.config

#rm check_errors.txt

#Compress Bam to Cram
if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then cd $outdir
        ls Preprocessing/*/Recalibrated/*.bam > recal_bam_list.txt
        awk '{print $NF}' FS=/ recal_bam_list.txt | sed 's/\.recal\.bam//'  > recal_bam_names.txt
        sbatch --export=ALL,batch=$batch,outdir=$outdir /data/scratch/DGE/DUDGE/PREDIGEN/iwade/cram_processing/submit_cram_processing.sh
        cd $rundir; else echo "fail during rm" >> check_errors.txt; fi

#Remove Marked Dup Bams and Non-g.vcf's
if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then cd $outdir
	find . -type d -name "DuplicatesMarked" -exec rm -rf {} \;
       	find . -type d -name "HaplotypeCaller" -exec rm -rf {} \;
       	cd $rundir; else echo "fail during rm" >> check_errors.txt; fi


#Back Up Rest of Results
if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then sbatch --export=outdir=$outdir,batch=$batch /data/scratch/DGE/DUDGE/PREDIGEN/iwade/transfer_scripts/submit_transfer_sarek_vcall_reports.sh ; else echo "Can't move results" >> check_errors.txt; fi

#Back up Fastq
ls /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/fastq/*fastq.gz | xargs -n 1 basename  | grep -f /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_list_forprefetch.txt"$fastq_backup" > /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_fastq_"$fastq_backup".txt
if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then sbatch --export=fastq_backup=$fastq_backup /data/scratch/DGE/DUDGE/PREDIGEN/iwade/transfer_scripts/submit_fastq_transfer.sh ; else echo "Can't move results" >> check_errors.txt; fi

#Change File Permissions (if necessary)
#if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then chmod -R 777 /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarek_results/sarcoma/ICR_controls/EGAD00001001021/outputs_AG; else echo "fail during chmod Error" >> check_errors.txt; fi

#Remove Work Dir
if cat .nextflow.log | grep -q "Pipeline completed successfully" ; then rm -rf work; else echo "fail during rm" >> check_errors.txt; fi  
