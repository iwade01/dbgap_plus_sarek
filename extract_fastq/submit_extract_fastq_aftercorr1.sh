#!/bin/bash
#SBATCH --job-name=sra_extract_fastq_aftercorr1
#SBATCH --output=slurm-%x-%A_%a.out
#SBATCH --partition=data-transfer
#SBATCH --ntasks=1
#SBATCH --time=72:00:00
#SBATCH --mail-user=isaac.wade@icr.ac.uk
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --array=1-500

#mkdir -p /data/rds/DGE/DUDGE/PREDIGEN/TIER2/iwade/fastq/testis/plat_res

#still testing - add --remove-source-files

#want this to be automatic
FILE_NAME=$(sed "${SLURM_ARRAY_TASK_ID}q;d" /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_list_forprefetch.txtap)

rsync -avP /data/rds/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data/"$FILE_NAME" \
       /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data
