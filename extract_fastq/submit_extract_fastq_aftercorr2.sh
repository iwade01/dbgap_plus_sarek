#!/bin/bash
#SBATCH --job-name=aric_extract_fastq_aftercorr2
#SBATCH --ntasks=1
#SBATCH --time=05:30:00
#SBATCH --partition=compute
#SBATCH --output=slurm-%x-%A_%a.out
#SBATCH --mail-user=isaac.wade@icr.ac.uk
#SBATCH --mail-type=ALL
#SBATCH --array=1-500%20
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=2000

. "/opt/software/applications/anaconda/3/etc/profile.d/conda.sh"

conda activate sra-tools2.10.8

sleep $(( RANDOM/300 ))

sra=$(sed "${SLURM_ARRAY_TASK_ID}q;d" /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_list_forprefetch.txtap)
tozip=$(sed "${SLURM_ARRAY_TASK_ID}q;d" /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_list_forprefetch.txtap)

#make/check tmp folder for extaction
mkdir -p /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/fastq
mkdir -p /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/decrypt_tmp/"$tozip"

#remove .sra extension (.sra causes errors)
mv /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data/"$sra"/"$sra".sra \
	/data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data/"$sra"/"$sra"

#Decrypt/extract
for i in {1..10}; do fasterq-dump --ngc /data/scratch/DGE/DUDGE/PREDIGEN/agarrett/dbgap/prj_29740.ngc -e 4 /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data/"$sra"/"$sra" \
        -O /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/fastq \
        -t /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/decrypt_tmp/"$tozip" && break || sleep 15; done

#Compress fastq, if paired
cd /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/fastq

if [ -f "$tozip"_1.fastq ]
then
	gzip -f  "$tozip"_1.fastq
	gzip -f  "$tozip"_2.fastq
	echo 'compressed and sorted'
else
	rm "$tozip".fastq
	echo 'it was unpaired, so it is gone'
fi

rm -rf /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data/"$sra"
