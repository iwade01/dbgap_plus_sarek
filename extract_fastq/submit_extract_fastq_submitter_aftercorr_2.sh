#!/bin/bash
#SBATCH --job-name=submitter_aric_extract_fastq
#SBATCH --output=slurm-%x-%A_%a.out
#SBATCH --partition=compute
#SBATCH --ntasks=1
#SBATCH --time=1:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50
#SBATCH --mail-user=isaac.wade@icr.ac.uk
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --dependency=afterany:10297482

sbatch submit_extract_fastq_aftercorr2.sh
