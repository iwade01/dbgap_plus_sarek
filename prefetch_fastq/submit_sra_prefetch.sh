#!/bin/bash
#
#SBATCH --job-name=aric_prefetch_rds_ax
#SBATCH --output=outfiles/slurm-%x-%A_%a.out
#SBATCH --mail-user=isaac.wade@icr.ac.uk
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --time=2:00:00
#SBATCH --partition=data-transfer
#SBATCH --array=1-500%25
#SBATCH --dependency=afterany:9906757

. "/opt/software/applications/anaconda/3/etc/profile.d/conda.sh"

conda activate sra-tools2.10.8

sra=$(sed "${SLURM_ARRAY_TASK_ID}q;d" /data/scratch/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_list_forprefetch.txtax)

mkdir -p /data/rds/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data
mkdir -p outfiles
cd /data/rds/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data

sleep $(( RANDOM/300 ))

for i in {1..10}; do prefetch --ngc /data/scratch/DGE/DUDGE/PREDIGEN/agarrett/dbgap/prj_29740.ngc $sra \
        -O /data/rds/DGE/DUDGE/PREDIGEN/iwade/sarcoma/dbgap_aric/sra_data \
       	&& break || sleep 15; done
